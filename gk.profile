<?php
/**
 * @file
 * Site configuration for GK site installation.
 */

/**
 * Implements hook_form_FORM_ID_alter() for install_select_profile_form().
 *
 * Pretending to be the Drupal core 'system' module for this hook as
 * hook_form_alter doesn't work at this stage of the install process.
 */
function system_form_install_select_profile_form_alter(&$form, $form_state) {
  // Remove Drupal core profile options.
  unset($form['profile']['Standard']);
  unset($form['profile']['Minimal']);

  // Set default profile to 'gk'.
  $form['profile']['GK']['#value'] = 'gk';
}


/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function gk_form_install_configure_form_alter(&$form, $form_state) {
  // Set the Seven logo to be GK's logo
  $theme_data = _system_rebuild_theme_data();
  $seven_data = $theme_data['seven']->info['settings'];
  $seven_data['default_logo'] = 0;
  $seven_data['logo_path'] = 'profiles/gk/logo.png';  variable_set('theme_seven_settings', $seven_data);

  // Hide non-important messages from contrib modules.
  drupal_get_messages('status');
  drupal_get_messages('warning');

  // Set sensible defaults for site configuration form
  $form['site_information']['site_name']['#default_value'] = $_SERVER['SERVER_NAME'];
  $form['admin_account']['account']['name']['#default_value'] = 'admin';
  $form['server_settings']['site_default_country']['#default_value'] = 'GB';
  $form['server_settings']['date_default_timezone']['#default_value'] = 'Europe/London';
}
